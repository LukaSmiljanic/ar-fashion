﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ArFashion.Models.ViewModels
{
    public class ProjectViewModel
    {
        public Project Project { get; set; }
    }
}
