﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ArFashion.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string  Name { get; set; }
        public string Image { get; set; }
        public string Model { get; set; }

    }
}
