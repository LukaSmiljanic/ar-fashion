﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ArFashion.Migrations
{
    public partial class AddImageColumnToProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Image",
                table: "Project",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "Project");
        }
    }
}
