﻿using ArFashion.Data;
using ArFashion.Models;
using ArFashion.Models.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ArFashion.Controllers
{
    public class ProjectController : Controller
    {
        private readonly ApplicationDbContext _db;
        private readonly IWebHostEnvironment _webHostEnvirnoment;


        public ProjectController(ApplicationDbContext db, IWebHostEnvironment webHostEnvironment)
        {
            _db = db;
            _webHostEnvirnoment = webHostEnvironment;
        }

        public IActionResult Index()
        {
            IEnumerable<Project> objList = _db.Project;
            return View(objList);
        }

        //Get-Create
        public IActionResult Create()
        {
            return View();
        }

        //Post-Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreatedProject(Project pvm)
        {
            //Collecting files from form
            var files = HttpContext.Request.Form.Files;
            string webRootPath = _webHostEnvirnoment.WebRootPath;

            string upload = webRootPath + WC.ImagePath;
            string fileName = Guid.NewGuid().ToString();

            string uploadModel = webRootPath + WC.ModelPath;
            string fileModelName = Guid.NewGuid().ToString();

            if(pvm.Name == null)
            {
                ModelState.AddModelError("Name", "Please insert project name");
                return View("Create");
            }

            if (files.Count() > 1)
            {
                string extension = Path.GetExtension(files[0].FileName);
                string extensionModel = Path.GetExtension(files[1].FileName);

                if (extension == ".jpg" || extension == ".jpeg" || extension == ".png")
                {
                    if (extensionModel == ".gltf")
                    {

                        using (var fileStream = new FileStream(Path.Combine(upload, fileName + extension), FileMode.Create))
                        {
                            files[0].CopyTo(fileStream);
                            files[1].CopyTo(fileStream);

                        }

                        using (var fileStream = new FileStream(Path.Combine(uploadModel, fileModelName + extensionModel), FileMode.Create))
                        {
                            files[1].CopyTo(fileStream);
                        }

                        pvm.Model = fileModelName + extensionModel;
                        pvm.Image = fileName + extension;

                        _db.Project.Add(pvm);
                        _db.SaveChanges();

                        RedirectToAction("Index");
                    }
                    else
                    {

                    ModelState.AddModelError("Model", "Please insert image with extension (jpg, jpeg, png) and model with extension (gltf)");
                    return View("Create");

                    }
                }
                else
                {
                    ModelState.AddModelError("Image", "Please insert image with extension (jpg, jpeg, png) and model with extension (gltf)");
                    return View("Create");
                }
            }
            else
            {

                ModelState.AddModelError("Model", "Please insert image and model");
                return View("Create");
            }

            return RedirectToAction("Index");
        }

        public IActionResult Delete(int id)
        {
            Project pr = new Project();
            pr = _db.Project.Find(id);

            string webRootPath = _webHostEnvirnoment.WebRootPath;

            string filePath = webRootPath + WC.ImagePath + pr.Image;
            FileInfo file = new FileInfo(filePath);
            if (file.Exists)
            {
                file.Delete();
            }

            string fileModelPath = webRootPath + WC.ModelPath + pr.Model;
            FileInfo fileModel = new FileInfo(fileModelPath);
            if (fileModel.Exists)
            {
                fileModel.Delete();
            }

            _db.Project.Remove(pr);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        //Get-Create
        public IActionResult ViewCreated(int id)
        {
            Project project = new Project();
            project = _db.Project.Find(id);

            IEnumerable<Project> projects = _db.Project;
            ViewBag.projects = projects;

            return View(project);
        }

        [HttpGet]
        public IActionResult ViewPartial(int id)
        {
            Project project = new Project();
            project = _db.Project.Find(id);

            return PartialView("_ARPartialView", project);

        }

    }
}
